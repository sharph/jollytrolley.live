FROM node:14 as BUILD

RUN mkdir /jolly-trolley

COPY . /jolly-trolley/

WORKDIR /jolly-trolley/

RUN npm install
RUN npm run build

FROM nginx:stable-alpine

RUN rm /usr/share/nginx/html/*
COPY --from=BUILD /jolly-trolley/build/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
