
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { Icon } from 'leaflet';
import React, { Component } from 'react';
import $ from 'jquery';
import _ from 'lodash';

import './App.css';

var TILE_URL = 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png';
var TILE_ATTRIBUTION = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>';
var CENTER = [39.952583, -75.165222];

var JOLLEY_TROLLEYS = ["9053", "9074"];  // https://twitter.com/ChrisMejiaSmith/status/1469726548722950159

var REFRESH_SECONDS = 20;

var icon = new Icon({iconUrl: '/hat5.gif', size: [50,40]});

function JollyTrolleyMarker (props) {
  return (
    <Marker position={[parseFloat(props.trolley.lat), parseFloat(props.trolley.lng)]} icon={icon} >
      <Popup>
        <div>
          <div><b>Route:</b> {props.trolley.route}</div>
          <div><b>Vehicle ID:</b> {props.trolley.VehicleID}</div>
        </div>
      </Popup>
    </Marker>
  );
}

function JollyTrolleyMap (props) {
  return (
    <Map zoom={13} center={CENTER}>
      <TileLayer
        attribution={TILE_ATTRIBUTION}
        url={TILE_URL}
        subdomains="abcd"
        maxZoom={19}
      />
      {props.trolleys.map(trolley => (<JollyTrolleyMarker trolley={trolley} key={trolley.VehicleID} />))}
    </Map>
  );
}

function filterData(routes) {
  return(_.chain(routes)
          .map((vehicles, route) => (
            _.map(vehicles, vehicle => {
              var newVehicle = vehicle;
              newVehicle.route = route;
              return newVehicle;
            })
          ))
          .values()
          .flatten()
          .filter(vehicle => (JOLLEY_TROLLEYS.includes(vehicle.VehicleID)))
          .value());
}

function Overlays(props) {
  return (
    <div id="overlay">
      {props.noTrolleys ? <div class="bigoverlay"><span class="fancycolor">Sorry! No jolly trolleys on the map right now!</span></div> : <></>}
    </div>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {loaded: false, vehicles: []};
    this.handleVisibilityChange = this.handleVisibilityChange.bind(this);
  }

  handleVisibilityChange() {
    if (document.visibilityState === "visible") {
      clearInterval(this.updateTimer);
      this.updateTimer = setInterval(() => this.pullNewData(), REFRESH_SECONDS * 1000);
      this.pullNewData();
    };
  }

  componentDidMount() {
    this.pullNewData();
    this.updateTimer = setInterval(() => this.pullNewData(), REFRESH_SECONDS * 1000);
    document.addEventListener("visibilitychange", this.handleVisibilityChange);
  }

  componentWillUnmount() {
    clearInterval(this.updateTimer);
    document.removeEventListener("visibilitychange", this.handleVisibilityChange);
  }

  pullNewData() {
    if (document.visibilityState !== "visible") {
      return;
    }
    $.ajax({
        url: "https://www3.septa.org/hackathon/TransitViewAll/",
        jsonp: "callback",
        dataType: "jsonp",
        success: (newData) => {
            this.setState({
              vehicles: filterData(newData.routes[0]),
              loaded: true
            });
        }
    });
  }

  render() {
    return (
      <>
      <JollyTrolleyMap trolleys={this.state.vehicles} />
      <Overlays noTrolleys={this.state.vehicles.length === 0}/>
      </>
    );
  }
}

export default App;
